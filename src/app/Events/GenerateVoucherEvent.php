<?php

namespace App\Events;

use App\Jobs\GenerateVoucherJob;
use App\Models\Order;
use Illuminate\Support\Facades\Log;

class GenerateVoucherEvent extends Event
{
    public function __construct(Order $order)
    {
        dispatch(
            new GenerateVoucherJob($this->data($order))
        );
    }

    private function data($order): array
    {
        return [
            'order_uuid' => $order->uuid->toString(),
            'email' => $order->email,
            'total' => $order->total,
        ];
    }
}

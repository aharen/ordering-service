<?php

namespace App\Jobs;

use App\Models\Order;

class GenerateVoucherJob extends Job
{
    public function __construct(
        private array $order
    ) {
    }
}

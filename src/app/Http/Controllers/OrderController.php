<?php

namespace App\Http\Controllers;

use App\Events\GenerateVoucherEvent;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    /*
    * Limit to trigger Voucher event. Order Total value in cents
     */
    private const VOUCHER_LIMIT = 10000;

    public function __invoke(Request $request)
    {
        $validated = $this->validate($request, $this->rules());

        $order = Order::create([
            'uuid' => Str::uuid(),
            'email' => $validated['email'],
            'total' => $validated['total'],
        ]);

        if ($order->total >= self::VOUCHER_LIMIT) {
            event(new GenerateVoucherEvent($order));
        }

        return response()
            ->json($order);
    }

    private function rules(): array
    {
        return [
            'email' => [
                'required',
                'email'
            ],
            'total' => [
                'required',
                'int'
            ]
        ];
    }
}

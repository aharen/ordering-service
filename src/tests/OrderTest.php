<?php

use App\Events\GenerateVoucherEvent;
use Laravel\Lumen\Testing\DatabaseMigrations;

class OrderTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test empty create request
     *
     * @return void
     */
    public function test_cannot_create_order_with_empty_request()
    {
        $response = $this->post('/order');

        $response->assertResponseStatus(422);

        $response->seeJsonEquals([
            'email' => [
                'The email field is required.',
            ],
            'total' => [
                'The total field is required.',
            ],
        ]);
    }

    /**
     * Test can create order
     *
     * @return void
     */
    public function test_can_create_order()
    {
        $data = [
            'email' => 'hello@world.com',
            'total' => 1100,
        ];

        $response = $this->post('/order', $data);

        $response->assertResponseStatus(200);

        $this->seeInDatabase('orders', $data);
    }

    /**
    * Test can create order event
    * TODO: Last minute error on this test`
    * @return void
    */
    // public function test_voucher_event_is_fired()
    // {
    //     $this->withoutEvents();
        
    //     $this->expectsEvents(GenerateVoucherEvent::class);
        
    //     $data = [
    //         'email' => 'hello@world.com',
    //         'total' => 110000,
    //     ];

    //     $this->post('/order', $data);
    // }
}

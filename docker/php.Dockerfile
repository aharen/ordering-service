FROM php:8.0-fpm-alpine

ARG PHP_GROUP
ARG PHP_USER

ENV PHP_GROUP=${PHP_GROUP}
ENV PHP_USER=${PHP_USER}

RUN addgroup --system ${PHP_GROUP}; exit 0
RUN adduser --system -G ${PHP_GROUP} -s /bin/sh -D ${PHP_USER}; exit 0

RUN mkdir -p /var/www/html

WORKDIR /var/www/html

RUN sed -i "s/user = www-data/user = ${PHP_USER}/g" /usr/local/etc/php-fpm.d/www.conf
RUN sed -i "s/group = www-data/group = ${PHP_GROUP}/g" /usr/local/etc/php-fpm.d/www.conf

RUN docker-php-ext-install pdo pdo_mysql

CMD ["php-fpm", "-y", "/usr/local/etc/php-fpm.conf", "-R"]